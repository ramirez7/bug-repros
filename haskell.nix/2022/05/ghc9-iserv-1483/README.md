Fix and bug can be selected with `--arg fix true/false`:

```
$ nix-build -A projectCross.mingwW64.hsPkgs.ghc9-iserv1483.components.exes.ghc9-iserv1483

# .... a lot of output ....
# ...

Preprocessing executable 'ghc9-iserv1483' for ghc9-iserv1483-0.1.0.0..
Building executable 'ghc9-iserv1483' for ghc9-iserv1483-0.1.0.0..
[1 of 1] Compiling Main             ( app/Main.hs, dist/build/ghc9-iserv1483/ghc9-iserv1483-tmp/Main.o )
---> Starting remote-iserv on port 7394
---| remote-iserv should have started on 7394
0048:err:systray:initialize_systray Could not create tray window
0084:err:setupapi:SetupDefaultQueueCallbackW copy error 1812 L"@C:\\windows\\system32\\drivers\\wineusb.sys,-1" -> L"C:\\windows\\inf\\wineusb.inf"
wine: configuration in L"/build" has been updated.
0024:err:module:import_dll Library libgcc_s_seh-1.dll (which is needed by L"Z:\\build\\tmp.nwJuPFIAL9\\remote-iserv.exe") not found
0024:err:module:LdrInitializeThunk Importing dlls for L"Z:\\build\\tmp.nwJuPFIAL9\\remote-iserv.exe" failed, status c0000135

$ nix-build -A projectCross.mingwW64.hsPkgs.ghc9-iserv1483.components.exes.ghc9-iserv1483 --arg fix true
# build succeeds...

$ wine64 ./result/bin/ghc9-iserv1483.exe
Hello, Haskell!

# Fixed upstream!
$ nix-build -A projectCross.mingwW64.hsPkgs.ghc9-iserv1483.components.exes.ghc9-iserv1483 --arg upstreamFix true
# build succeeds...

$ wine64 ./result/bin/ghc9-iserv1483.exe
Hello, Haskell!

```
- Upstream issues/PRs:
  - https://github.com/input-output-hk/haskell.nix/issues/1483
