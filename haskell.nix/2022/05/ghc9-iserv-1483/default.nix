{ fix ? false, upstreamFix ? false }:
let
  pkgs = import ./nix/pkgs.nix { inherit fix upstreamFix; };
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "ghc9-iserv1483";
    src = ./../../../..;
    subDir = "haskell.nix/2022/05/ghc9-iserv-1483";
  };

  compiler-nix-name = if upstreamFix then "ghc923" else "ghc922";
}
