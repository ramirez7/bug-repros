{ fix ? false, upstreamFix ? false }:
let
  pkgs = import ./nix/pkgs.nix { inherit fix upstreamFix; };
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "libm-1128";
    src = ./../../../..;
    subDir = "haskell.nix/2022/05/libm-1128";
  };

  compiler-nix-name = if upstreamFix then "ghc923" else "ghc922";

  modules = [{
    # See https://github.com/input-output-hk/haskell.nix/issues/1177#issuecomment-891568396
    #
    # hspec-core is the offender .. this is a big hammer but it does work
    nonReinstallablePkgs = [
      "rts" "ghc-heap" "ghc-prim" "integer-gmp" "integer-simple" "base"
      "deepseq" "array" "ghc-boot-th" "pretty" "template-haskell"
      # ghcjs custom packages
      "ghcjs-prim" "ghcjs-th"
      "ghc-bignum" "exceptions" "stm"
      "ghc-boot"
      "ghc" "Cabal" "Win32" "array" "binary" "bytestring" "containers"
      "directory" "filepath" "ghc-boot" "ghc-compact" "ghc-prim"
      # "ghci" "haskeline"
      "hpc"
      "mtl" "parsec" "process" "text" "time" "transformers"
      "unix" "xhtml" "terminfo"
    ];
  }];
}
