You can toggle the fixes with a Nix arg:


```
$ nix-build -A libm1128.components.exes.libm1128
trace: No index state specified for haskell-project, using the latest index state that we know about (2022-05-24T00:00:00Z)!
error: The Nixpkgs package set does not contain the package: m (system dependency).
       You may need to augment the system package mapping in haskell.nix so that it can be found.
(use '--show-trace' to show detailed location information)

$ nix-build -A libm1128.components.exes.libm1128 --arg fix true
trace: No index state specified for haskell-project, using the latest index state that we know about (2022-05-24T00:00:00Z)!
/nix/store/jakm9cikv5ijrjys8dvqgr7js0bhahh8-libm1128-exe-libm1128-0.1.0.0

$ ./result/bin/libm1128 
Hello, hgeometry!Line (Point2 0 100) (Vector2 1 0)

# It is now fixed upstream!
$ nix-build -A libm1128.components.exes.libm1128 --arg upstreamFix true
```

Upstream issues/PRs:

- https://github.com/input-output-hk/haskell.nix/issues/1128
- https://github.com/input-output-hk/haskell.nix/pull/1453
