{ fix ? false, upstreamFix ? false }:
let
  sources = import ./sources.nix {};
  upstream = if upstreamFix then import sources.haskellNixUpstreamFix {} else import sources.haskellNixUpstreamBroken;
  haskellNix = if fix then import sources.haskellNixFix {} else upstream;
  pkgs = import haskellNix.sources.nixpkgs-unstable haskellNix.nixpkgsArgs;
in
pkgs
