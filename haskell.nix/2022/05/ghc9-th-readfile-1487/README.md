```bash
nix-build -A projectCross.mingwW64.hsPkgs.ghc9-th-readfile1487.components.exes.prelude-readFile --arg upstreamFix true
# it works!
```

The current rev fixes it with a hack (pinning wine to 3.21): https://github.com/input-output-hk/haskell.nix/pull/1508
