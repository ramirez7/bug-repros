{ fix ? false, upstreamFix ? false, ghc ? "ghc923" }:
let
  pkgs = import ./nix/pkgs.nix { inherit fix upstreamFix; };
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "ghc9-th-readfile1487";
    src = ./../../../..;
    subDir = "haskell.nix/2022/05/ghc9-th-readfile-1487";
  };

  compiler-nix-name = ghc;
}
