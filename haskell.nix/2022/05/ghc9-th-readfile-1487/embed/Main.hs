{-# LANGUAGE TemplateHaskell #-}

module Main where

import qualified Language.Haskell.TH.Syntax as TH
import Data.FileEmbed

main :: IO ()
main = putStrLn $(embedFile "data/foo.txt")

