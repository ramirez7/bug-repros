{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}

module Main where
import Control.Monad.Trans.Fault

main :: IO ()
main = do
  str <- runFaultlessT $ faulty @"uh oh" $ pure "Hello, monad-fault!"
  putStrLn str
