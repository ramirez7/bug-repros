{ exactDeps ? true, withShellHook ? false, homeDir ? builtins.getEnv "HOME" }:
let pkgs = import ./nix/pkgs.nix;
    project = import ./default.nix;
    mkCabalConfig = { configFiles }:
      let originalConfig = builtins.readFile "${configFiles}/cabal.config";
      in pkgs.writeText "cabal.config" ''
         ${originalConfig}
         package-db: ${homeDir}/.cabal/store/ghc-${project.ghc.identifier.version}/package.db
      '';
    mkShell = shellHook: project.shellFor {
      tools = {
        cabal = "latest";
        hsc2hs = "latest";
      };
      inherit exactDeps;
      withHoogle = false;
      inherit shellHook;
    };
    baseShell = mkShell "";
    fixedShellHook = baseShell.shellHook + pkgs.lib.optionalString withShellHook ''
      export CABAL_CONFIG=${mkCabalConfig { configFiles = baseShell.configFiles; }}
    '';
    fixedShell = mkShell fixedShellHook;
in fixedShell
