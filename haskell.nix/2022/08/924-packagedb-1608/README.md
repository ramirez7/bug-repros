`nix-shell` will give you a shell that reproduces the issue.

```
$ cabal repl
# If a package.db does not already exists (such as after rm -rf ~/.cabal)
# ...
Installing library in /home/armando/.cabal/store/ghc-9.2.4/incoming/new-673026/home/armando/.cabal/store/ghc-9.2.4/monad-fault-0.1.0.0-e935a3cf1991cc93acd85eea38d17939ef6486db7172b43d35caa5fdb1880a07/lib
Error: cabal: Failed to build monad-fault-0.1.0.0 (which is required by
exe:ghc924-package-db-1608 from ghc924-package-db1608-0.1.0.0). The failure
occurred during the final install step. The exception was:
/home/armando/.cabal/store/ghc-9.2.4/package.db/:
openBinaryTempFileWithDefaultPermissions: does not exist (No such file or
directory

# If a package.db already exists (such as after the workaround)
# ...
Error: cabal: The following package dependencies were requested
--dependency='monad-fault=monad-fault-0.1.0.0-e935a3cf1991cc93acd85eea38d17939ef6486db7172b43d35caa5fdb1880a07'
however the given installed package instance does not exist.
```

`nix-shell --arg exactDeps false` will give you a workaround.
