let
  pkgs = import ./nix/pkgs.nix;
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "libm-1128";
    src = ./../../../..;
    subDir = "haskell.nix/2022/08/924-packagedb-1608";
  };

  compiler-nix-name = "ghc924";
}
