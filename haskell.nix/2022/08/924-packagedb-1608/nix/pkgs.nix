let
  sources = import ./sources.nix {};
  haskellNix = import sources.haskellNixUpstreamBroken {};
  pkgs = import haskellNix.sources.nixpkgs-unstable haskellNix.nixpkgsArgs;
in
pkgs
